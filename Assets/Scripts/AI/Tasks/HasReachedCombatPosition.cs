﻿using System.Collections.Generic;
using UnityEngine;

public class HasReachedCombatPosition : Task
{
    public HasReachedCombatPosition(BehaviorTree tree) : base(tree)
    {
    }

    public HasReachedCombatPosition(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree)
            return ETaskState.FAILURE;

        Blackboard board = _Tree.GetBlackboard();
        if (board != null)
        {
            board.SetValueAsBool("HasMoveToLocation", false);
            board.SetValueAsVector("MoveToLocation", Vector3.zero);
            board.SetValueAsBool("WantsToMoveLocation", false);
            return ETaskState.SUCCESS;
        }

        return ETaskState.FAILURE;
    }
}