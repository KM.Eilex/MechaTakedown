using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartTrigger : MonoBehaviour
{
    [SerializeField] private IntroDoorController _Door;

    [SerializeField] private SpawnDoorController _StartingDoorOne;
    [SerializeField] private SpawnDoorController _StartingDoorTwo;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _Door.TriggerCloseDoor();
            SpawnController.Instance.AddDoor(_StartingDoorOne);
            SpawnController.Instance.AddDoor(_StartingDoorTwo);
        }
    }
}
