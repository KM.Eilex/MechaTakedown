using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AmmoPickup : MonoBehaviour
{
    private int _AmmoAmount;

    public void Setup(int amt)
    {
        _AmmoAmount = amt;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController player = other.GetComponent<PlayerController>();
            if (player != null)
            {
                if (player.HasWeaponOne && player.HasWeaponTwo)
                {
                    int weaponIndex = Random.Range(1, 3);
                    if(weaponIndex == 1)
                        player.WeaponOne.AddAmmo(_AmmoAmount);
                    else 
                        player.WeaponTwo.AddAmmo(_AmmoAmount);
                } else if (player.HasWeaponOne)
                {
                    player.WeaponOne.AddAmmo(_AmmoAmount);
                }
                else
                {
                     if(player.WeaponTwo)
                         player.WeaponTwo.AddAmmo(_AmmoAmount);
                }
                
                Destroy(this.gameObject);
            }
        }
    }
}
