﻿using UnityEngine;
using System.Collections.Generic;

public class DatabaseController : MonoBehaviour
{
    public static DatabaseController Instance;
    [SerializeField] private List<GameObject> _Weapons = new List<GameObject>();
    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public GameObject GetWeaponByName(string weaponName)
    {
        foreach (var weapon in _Weapons)
        {
            if (weapon.GetComponent<WeaponController>()?.WeaponName == weaponName)
                return weapon;
        }

        return null;
    }
}