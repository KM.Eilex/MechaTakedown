﻿using System.Collections.Generic;
using UnityEngine;

public class CobraBT : BehaviorTree
{
    protected override Task CreateTree()
    {
        return new Selector(this, new List<Task>
        {
            new Sequence(this, new List<Task>
            {
                new HasTarget(this),
                new Selector(this, new List<Task>
                {
                    new Sequence(this, new List<Task>
                    {
                        new IsInAttackRange(this),
                        new AttackTask(this)
                    }),
                    new Sequence(this, new List<Task>
                    {
                        new MoveTowardsTarget(this),
                        new MoveToLocation(this)
                    })
                })
            }),
            new Sequence(this, new List<Task>
            {
                new IsWaiting(this),
                new WaitTask(this, 2.0f)
            }),
            new Sequence(this, new List<Task>
            {
                new IsFollowingPath(this),
                new GetPathPointTask(this),
                new MoveToLocation(this),
                new NextPathPointTask(this, true)
            }),
            new Sequence(this, new List<Task>
            {
                new HasMoveToLocation(this),
                new MoveToLocation(this)
            })
        });
    }
}