﻿using UnityEngine;
using System.Collections.Generic;

public class WantsToMoveLocation : Task
{
    public WantsToMoveLocation(BehaviorTree tree) : base(tree)
    {
    }

    public WantsToMoveLocation(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree)
            return ETaskState.FAILURE;

        if (_Tree.GetBlackboard().GetValueAsBool("WantsToMoveLocation"))
            return ETaskState.SUCCESS;

        return ETaskState.FAILURE;
    }
}