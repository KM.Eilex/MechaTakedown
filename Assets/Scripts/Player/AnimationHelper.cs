﻿using UnityEngine;

public class AnimationHelper : MonoBehaviour
{
    [SerializeField] private PlayerController _Player;              // Store reference to the player controller


    public void OnFinishReload()
    {
        if(_Player)
            _Player.FinishReload();
    }
}