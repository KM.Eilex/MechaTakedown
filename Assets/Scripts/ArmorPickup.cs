using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ArmorPickup : MonoBehaviour
{
    private float _ArmorPercentage = 1.0f;

    public void Setup(float amt)
    {
        _ArmorPercentage += amt;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController player = other.GetComponent<PlayerController>();
            if (player != null)
            {
                player.AddArmor(_ArmorPercentage);
            }
            
            Destroy(this.gameObject);
        }
    }
}
