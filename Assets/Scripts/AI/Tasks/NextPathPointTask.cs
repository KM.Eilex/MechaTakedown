﻿using UnityEngine;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

public enum EMoveDirection
{
    FORWARD,
    BACKWARDS,
    BOTH
}

public class NextPathPointTask : Task
{
    private EMoveDirection _CurrentMovementDirection = EMoveDirection.FORWARD;
    private bool _WaitAtEnd = false;

    public NextPathPointTask(BehaviorTree tree, bool waitAtEnd = false) : base(tree)
    {
        _WaitAtEnd = waitAtEnd;
    }
    

    public NextPathPointTask(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree || _Tree.GetBlackboard() == null)
            return ETaskState.FAILURE;

        int currentIndex = _Tree.GetBlackboard().GetValueAsInt("CurrentPathIndex");
        AIController controller = _Tree.Owner;

        if (controller != null)
        {
            if (controller.Path.Circle)
            {
                currentIndex += 1;
                if (currentIndex > controller.Path.PathPointCount)
                {
                    if(_WaitAtEnd)
                        _Tree.GetBlackboard().SetValueAsBool("Wait", true);
                    currentIndex = 0;
                }
                    

                _Tree.GetBlackboard().SetValueAsInt("CurrentPathIndex", currentIndex);
                
                
                
                return ETaskState.SUCCESS;
            }
            else
            {
                if (_CurrentMovementDirection == EMoveDirection.FORWARD)
                {
                    currentIndex += 1;
                    if (currentIndex > controller.Path.PathPointCount)
                    {
                        _CurrentMovementDirection = EMoveDirection.BACKWARDS;
                        currentIndex -= 2;
                        _Tree.GetBlackboard().SetValueAsInt("CurrentPathIndex", currentIndex);
                        if(_WaitAtEnd)
                            _Tree.GetBlackboard().SetValueAsBool("Wait", true);
                        
                        return ETaskState.SUCCESS;
                    }
                    else
                    {
                        currentIndex -= 1;
                        if (currentIndex < 0)
                        {
                            _CurrentMovementDirection = EMoveDirection.FORWARD;
                            currentIndex += 2;
                            _Tree.GetBlackboard().SetValueAsInt("CurrentPathIndex", currentIndex);
                            if(_WaitAtEnd)
                                _Tree.GetBlackboard().SetValueAsBool("Wait", true);
                            
                            return ETaskState.SUCCESS;
                        }
                    }
                }
            }
        }

        return ETaskState.FAILURE;
    }
}