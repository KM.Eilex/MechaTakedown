﻿using UnityEngine;

public class BlackboardDebugger : MonoBehaviour
{
    [SerializeField] private BehaviorTree _Owner;
    private GameObject _Player;
    private Blackboard _Board;
    
    public bool HasMoveTo;
    public Vector3 MoveToLocation;
    public bool Wait;
    public bool WantsToMove;
    public float DistanceToPlayer;
    public bool HasTarget;

    private void Start()
    {
        _Board = _Owner.GetBlackboard();
        _Player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        HasMoveTo = _Board.GetValueAsBool("HasMoveToLocation");
        MoveToLocation = _Board.GetValueAsVector("MoveToLocation");
        Wait = _Board.GetValueAsBool("Wait");
        WantsToMove = _Board.GetValueAsBool("WantsToMoveLocation");
        DistanceToPlayer = Vector3.Distance(this.transform.position, _Player.transform.position);
        HasTarget = _Board.GetValueAsGameObject("Target");
    }
}