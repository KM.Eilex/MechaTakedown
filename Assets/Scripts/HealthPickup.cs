using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    private float _RestorePercentage;
    
    public void Setup(float amt = 0.25f)
    {
        _RestorePercentage = 1 + amt;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController player = other.GetComponent<PlayerController>();
            
            if(player)
                player.AddHealthPercentage(_RestorePercentage);
            
            Destroy(this.gameObject);
        }
    }
}
