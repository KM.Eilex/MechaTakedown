﻿using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class WeaponController : MonoBehaviour
{
    [Header("Weapon Details")]
    public string WeaponName;
    public string WeaponDescription;
    public EWeaponType WeaponType;
    public EFireType FireType;

    [Header("Fire Properties")] 
    public float DamagePoints;
    public float FireCooldown;
    [SerializeField] private Transform _FirePoint;
    [SerializeField] private GameObject _BulletPrefab;

        [Header("Muzzle")]
    [SerializeField] private GameObject _MuzzleFlash;
    [SerializeField] private float _FireTime;                   // How long the gun fires for

    [Header("Ammo Properties")]
    public int MaxAmmoInMag;
    private int _CurrentTotalAmmo = 60;                  // How much ammo we currently have
    private int _AmmoInMag = 30;                         // Ammo currently in 

    public Camera _Cam;


    [Header("Current Weapon State")] 
    private bool _CanFire = true;
    private bool _IsReloading = false;

    [Header("Audio")] 
    [SerializeField] private AudioSource _Audio;
    [SerializeField] private AudioClip _ShootSFX;

    public bool CanFire => _CanFire;

    private void Awake()
    {
        _Cam = Camera.main;
    }
    
    public void AddAmmo(int amt)
    {
        _CurrentTotalAmmo += amt;
    }

    public bool Fire(float hitModifier = 1.0f)
    {
        // Validate that we can fire
        if (_IsReloading || !_CanFire || _AmmoInMag == 0)
            return false;

        if (_Audio)
        {
            _Audio.clip = _ShootSFX;
            _Audio.Play();
        }
        
        _MuzzleFlash.SetActive(true);               // Enable the muzzle flash   
        
        _AmmoInMag -= 1;                            // Reduce the ammo
        _CanFire = false;                           // Prevent firing again

        // Cast a ray and detect if it hits an enemy
        if (Physics.Raycast(_Cam.transform.position, _Cam.transform.forward, out RaycastHit hit, 1000f))
        {
            if (hit.collider.gameObject.CompareTag("Enemy"))
            {
                AIController ai = hit.collider.GetComponent<AIController>();
                if(ai != null)
                    ai.TakeDamage(DamagePoints * hitModifier);
            }
        }

        // Reset weapon
        StartCoroutine(ToggleMuzzle());
        StartCoroutine(Cooldown());

        return true;
    }

    private IEnumerator ToggleMuzzle()
    {
        yield return new WaitForSeconds(_FireTime);
        _MuzzleFlash.SetActive(false);
    }

    public void Reload()
    {
        _IsReloading = true;
        // TODO: Toggle animation
    }

    public void FinishReload()
    {
        int ammoRequired = MaxAmmoInMag - _AmmoInMag;
        if (ammoRequired < _CurrentTotalAmmo)
        {
            _AmmoInMag = MaxAmmoInMag;
            _CurrentTotalAmmo -= ammoRequired;
        }
        else
        {
            _AmmoInMag += _CurrentTotalAmmo;
            _CurrentTotalAmmo = 0;
        }

        _IsReloading = false;
    }

    private IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(FireCooldown);
        _CanFire = true;
    }
    
    
}