﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour
{
    [Header("AI")] 
    protected Blackboard _Blackboard;                   // Reference to the blackboard

    public Blackboard BlackboardRef => _Blackboard;


    [Header("Attack Settings")] 
    [SerializeField] private float _MinAttackDistance;
    [SerializeField] private float _MaxAttackDistance;
    [SerializeField] private float _AttackCooldown;
    private bool _CanAttack = true;
    
    
    
    [Header("Components")] 
    [SerializeField] protected NavMeshAgent _Agent;                 // Reference to the navigation agent
    [SerializeField] protected BehaviorTree _Tree;                  // Reference to the behavior tree
    [SerializeField] protected Animator _Anim;                      // Reference to the animator
    [SerializeField] protected DropController _Drop;                // Reference to the drop controller that will handle dropping items

    [Header("Health stats")] 
    [SerializeField] private float _MaxHealth;                      // Max health the ai can have
    private float _CurrentHealth;                                   // Current state of the ai health
    [SerializeField] private float _LowHealth;                      // Reference to what is considered low health
    [SerializeField] private GameObject _SmokeParticle;
    public bool IsAlive => _MaxHealth > 0;

    [SerializeField] private Transform _DebugPosition;

    

    [Header("Path Following")] 
    [SerializeField] private PathPointController _Path;

    public PathPointController Path => _Path;

    protected virtual void Awake()
    {
        CreateBlackboard();
        _CurrentHealth = _MaxHealth;
    }

    protected virtual void Start()
    {
        // Initialize the tree by setting blackboard and this as the owner
        if(_Tree)
            _Tree.Initialize(_Blackboard, this);
    }

    /// <summary>
    /// Updates the move to location of the nav agent
    /// </summary>
    /// <param name="position">Position to move to</param>
    public void MoveTo(Vector3 position)
    {
        // Validate that we have an agent and set where the agent needs to move to
        if (_Agent)
        {
            _Agent.SetDestination(position);
            _Agent.isStopped = false;
        }
            
        // Update animations
        if(_Anim)
            _Anim.SetBool("IsMoving", true);
    }

    /// <summary>
    /// Stops the AI from moving
    /// </summary>
    public void StopMoving()
    {
        // Stop the agent from moving
        if (_Agent)
            _Agent.isStopped = true;
        
        // Update the animator
        if(_Anim)
            _Anim.SetBool("IsMoving", false);
    }

    /// <summary>
    /// Creates a mew blackboard with default properties
    /// </summary>
    protected virtual void CreateBlackboard()
    {
        _Blackboard = new Blackboard();
        _Blackboard.SetValueAsBool("HasMoveToLocation", false);
        _Blackboard.SetValueAsVector("MoveToLocation", Vector3.zero);
        _Blackboard.SetValueAsBool("FollowPath", _Path);
        _Blackboard.SetValueAsInt("CurrentPathIndex", 0);
        _Blackboard.SetValueAsBool("Wait", false);
        _Blackboard.SetValueAsFloat("MinAttackDistance", _MinAttackDistance);
        _Blackboard.SetValueAsFloat("MaxAttackDistance", _MaxAttackDistance);
        _Blackboard.SetValueAsBool("WantsToMoveLocation", false);
    }

    /// <summary>
    /// Takes damage away from this health property
    /// </summary>
    /// <param name="dmgAmt">Damage amount to take</param>
    public void TakeDamage(float dmgAmt)
    {
        _CurrentHealth -= dmgAmt;               // Reduce health
        // Kill enemy
        if (_CurrentHealth < 0)
        {
            _Tree._IsActive = false;                    // Disable the behavior
            // Update the animator to play death anim
            if(_Anim)
                _Anim.SetTrigger("Death");

            // Disable the capsule collider
            this.GetComponent<CapsuleCollider>().enabled = false;

            _Tree._IsActive = false;
            
            if(_Drop)
                _Drop.CreateDrop();

            if (_SmokeParticle)
                _SmokeParticle.SetActive(false);
        } else if (_CurrentHealth < _LowHealth)
        {
            if(_SmokeParticle)
                _SmokeParticle.SetActive(true);
        }
    }

    public virtual void AttackTarget(PlayerController target)
    {
        // Prevent attacking while in cooldown
        if (!_CanAttack)
            return;
        
        
        _CanAttack = false;                 // Prevent spamming attack
        // Timer to reset the cooldown
        StartCoroutine(WaitAttackCooldown());
        // Update the animator
        if (_Anim)
        {
            _Anim.SetInteger("AttackIndex", Random.Range(0, 2));
            _Anim.SetTrigger("Attack");
        }
    }

    private IEnumerator WaitAttackCooldown()
    {
        yield return new WaitForSeconds(_AttackCooldown);
        _CanAttack = true;
    }
    
}