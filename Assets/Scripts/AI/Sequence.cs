﻿using UnityEngine;
using System.Collections.Generic;

public class Sequence : Task
{
    public Sequence(BehaviorTree tree) : base(tree)
    {
    }

    public Sequence(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        foreach (var task in _ChildrenTask)
        {
            if (task != null)
            {
                switch (task.RunTask())
                {
                    case ETaskState.FAILURE:
                        return ETaskState.FAILURE;
                    case ETaskState.RUNNING:
                        return ETaskState.RUNNING;
                    case ETaskState.SUCCESS:
                        continue;
                }
            }
        }

        return ETaskState.FAILURE;
    }
}