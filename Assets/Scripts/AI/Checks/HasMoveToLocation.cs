﻿using System.Collections.Generic;
using UnityEngine;

public class HasMoveToLocation : Task
{
    public HasMoveToLocation(BehaviorTree tree) : base(tree)
    {
    }

    public HasMoveToLocation(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (_Tree == null || _Tree.GetBlackboard() == null)
        {
            Debug.Log("Tree or blackboard not set");
            return ETaskState.FAILURE;
        }
            

        if (_Tree.GetBlackboard().GetValueAsBool("HasMoveToLocation"))
        {
            Debug.Log("Has Move to location");
            return ETaskState.SUCCESS;
        }
            


        return ETaskState.FAILURE;
    }
}