﻿using UnityEngine;
using System.Collections.Generic;

public class DropController : MonoBehaviour
{
    [Header("Drop Chance")]
    [SerializeField] private float _ChanceOfAmmoDrop;
    [SerializeField] private float _ChanceOfHealthDrop;
    [SerializeField] private float _ChanceOfArmorDrop;

    [Header("Prefabs")] 
    [SerializeField] private GameObject _HealthPack;
    [SerializeField] private GameObject _AmmoPack;
    [SerializeField] private GameObject _ArmorPack;

    public void CreateDrop()
    {
        float ammoValue = Random.Range(0f, 1.1f);
        if (ammoValue < _ChanceOfAmmoDrop)
            DropAmmo();

        float healthValue = Random.Range(0f, 1.1f);
        if (healthValue < _ChanceOfHealthDrop)
            DropHealth();

        float armorDrop = Random.Range(0f, 1.1f);
        if (armorDrop < _ChanceOfArmorDrop)
            DropArmor();
    }

    private void DropAmmo()
    {
        int ammoAmount = Random.Range(1, 10);
        GameObject go = Instantiate(_AmmoPack);
        if (go != null)
        {
            go.transform.position = GetDropPosition();
            go.GetComponent<AmmoPickup>().Setup(ammoAmount);
        }
    }

    private void DropHealth()
    {
        float healthPercentage = Random.Range(0.1f, 0.35f);
        GameObject go = Instantiate(_HealthPack);
        if (go != null)
        {
            go.transform.position = GetDropPosition();
            go.GetComponent<HealthPickup>()?.Setup(healthPercentage);
        }
    }

    private void DropArmor()
    {
        float armor = Random.Range(0.1f, 0.2f);
        GameObject go = Instantiate(_ArmorPack);
        if (go)
        {
            go.transform.position = GetDropPosition();
            go.GetComponent<ArmorPickup>()?.Setup(armor);
        }
    }

    private Vector3 GetDropPosition()
    {
        return new Vector3
        {
            x = this.transform.position.x + Random.Range(-5, 5),
            y = this.transform.position.y + 0.5f,
            z = this.transform.position.z + Random.Range(-5, 5)
        };

        
    }

}