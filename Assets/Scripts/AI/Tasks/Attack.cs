﻿using System.Collections.Generic;
using UnityEngine;

public class AttackTask : Task
{
    public AttackTask(BehaviorTree tree) : base(tree)
    {
    }

    public AttackTask(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree)
            return ETaskState.FAILURE;

        GameObject target = _Tree.GetBlackboard().GetValueAsGameObject("Target");
        _Tree.Owner.AttackTarget(target.GetComponent<PlayerController>());
        return ETaskState.SUCCESS;
    }
}