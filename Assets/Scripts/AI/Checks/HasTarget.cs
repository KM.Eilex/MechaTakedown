﻿using UnityEngine;
using System.Collections.Generic;

public class HasTarget : Task
{
    public HasTarget(BehaviorTree tree) : base(tree)
    {
    }

    public HasTarget(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree)
            return ETaskState.FAILURE;


        if (_Tree.GetBlackboard().GetValueAsGameObject("Target"))
            return ETaskState.SUCCESS;

        return ETaskState.FAILURE;
    }
}