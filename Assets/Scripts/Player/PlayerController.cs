using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerController : MonoBehaviour
{
    [Header("Movement Settings")] 
    [SerializeField] private float _MovementSpeed;              // Standard movement speed
    [SerializeField] private float _SprintSpeed;                // Speed when sprinting
    
    [Header("State")]
    private bool _IsSprint = false;                         // If the character is currently sprinting
    private bool _IsAiming = false;                         // If the character is currently aiming
    private bool _IsFiring = false;                         // If the character is currently shooting
    private bool _IsReloading = false;                      // If the character is currently reloading

    [Header("Look Settings")] 
    [SerializeField] private Camera _Cam;                           // Reference to the players camera
    [SerializeField] private float _ClampXRotationMin;              // Min we want the x rotation to go to
    [SerializeField] private float _ClampXRotationMax;              // Max we want the x rotation to go to
    [SerializeField] private float _LookSensitivity;                // How sensitive the mouse is
    private float _XRotation = 0f;                                  // Reference to the current x rotation

    public Camera Cam => _Cam;
    
    [Header("Components")]
    [SerializeField] private Rigidbody _Rb;                         // Reference to the rigidbody
    [SerializeField] private Animator _Anim;                        // Reference to the animator
    [SerializeField] private RecoilComponent _Recoil;
    // === INPUT === //
    private PlayerGameInputs _Input;                                // Reference to the inputs 

    [Header("Spawn Settings")] 
    [SerializeField] private bool _SpawnWithWeapon = true;              // If we want to spawn with a weapon
    [SerializeField] private string _WeaponName;

    [Header("In-Hand Weapons")] 
    [SerializeField] private Transform _WeaponAttachPoint;                       // point at where the weapon attaches to
    private WeaponController _WeaponOne;
    [SerializeField] private GameObject _SpawnedWeaponOne;
    private WeaponController _WeaponTwo;
    [SerializeField] private GameObject _SpawnedWeaponTwo;
    private int _EquippedWeapon = 2;

    public WeaponController WeaponOne => _WeaponOne;
    public WeaponController WeaponTwo => _WeaponTwo;

    public bool HasWeaponOne => _WeaponOne != null;
    public bool HasWeaponTwo => _WeaponTwo != null;

    [Header("Health Stats")] 
    [SerializeField] private float _MaxHealth;
    private float _CurrentHealth;
    [SerializeField] private float _MaxArmour;
    private float _CurrentArmour;

    [Header("Combat Settings")] 
    [SerializeField] private float _ChanceOfCriticalHit = 0.2f;
    [SerializeField] private float _CriticalHitModifier = 1.25f;
    
    [Header("Recoil Settings")] 
    [SerializeField] private float _MinRecoilY;
    [SerializeField] private float _MaxRecoilY;
    [SerializeField] private float _RecoilX;
    [SerializeField] private float _RecoilSpeed;
    [SerializeField] private float _ReturnRecoilSpeed;
    private Vector3 _RecoilToApply;
    private bool _ApplyRecoil = false;
    private Vector3 _CurrentRecoil;

    [Header("Door Settings")] 
    [SerializeField] private float _RequiredDistanceFromDoor;                    // How far the character has to be to open a dor


    private void Awake()
    {
        _Input = new PlayerGameInputs();                // Create the input class
        
        // Validate components required
        if (!_Cam)
            _Cam = Camera.main;

        if (!_Rb)
            TryGetComponent<Rigidbody>(out _Rb);

        if (!_Anim)
            TryGetComponent<Animator>(out _Anim);

        _CurrentHealth = _MaxHealth;
        _CurrentArmour = _MaxArmour;
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        
        if (_SpawnWithWeapon)
        {
            GameObject spawningWeapon = DatabaseController.Instance.GetWeaponByName(_WeaponName);
            SetWeapon(spawningWeapon, 2);
        }
    }

    private void OnEnable()
    {
        if(_Input != null)
            _Input.Enable();
    }

    private void OnDisable()
    {
        if(_Input != null)
            _Input.Disable();
    }

    public void TakeDamage(float dmgAmt)
    {
        if (_CurrentArmour < 0)
        {
            _CurrentHealth -= dmgAmt;
        }
        else
        {
            float armourRemaining = dmgAmt - _CurrentArmour;
            if (armourRemaining > 0)
            {
                _CurrentArmour = 0f;
                _CurrentHealth -= armourRemaining;
            }
            else
            {
                _CurrentArmour -= dmgAmt;
            }
        }
    }

    public void AddHealthPercentage(float amt)
    {
        _CurrentHealth *= amt;
        if (_CurrentHealth > _MaxHealth)
            _CurrentHealth = _MaxHealth;
    }

    public void AddArmor(float amt)
    {
        _CurrentArmour *= amt;
        if (_CurrentArmour > _MaxArmour)
            _CurrentArmour = _MaxArmour;
    }

    private void Update()
    {
        if (_IsFiring)
        {
            // Determine critical hit
            float hitChanceValue = Random.Range(0f, 1f);
            float modifier = 1.0f;
            if (hitChanceValue < _ChanceOfCriticalHit)
                modifier = _CriticalHitModifier;
            
            if (_EquippedWeapon == 1 && _WeaponOne)
            {
                if (_WeaponOne.Fire(modifier))
                {
                    _ApplyRecoil = true;
                    _RecoilToApply += CreateRecoil();
                    StartCoroutine(StopRecoil(_WeaponOne.FireCooldown));

                    if(_Anim)
                        _Anim.SetTrigger("Fire");
                }
            } else if (_EquippedWeapon == 2 && _WeaponTwo)
            {
                if (_WeaponTwo.Fire(modifier))
                {
                    _ApplyRecoil = true;
                    _RecoilToApply += CreateRecoil();
                    StartCoroutine(StopRecoil(_WeaponTwo.FireCooldown));
                    if(_Anim)
                        _Anim.SetTrigger("Fire");
                }
            }
        }
        
        HandleRecoil();
        DetectOpenDoor();
    }

    private void FixedUpdate()
    {
        HandleMovement();
        HandleRotation();
    }


    /// <summary>
    /// Handles the movement of the character
    /// </summary>
    private void HandleMovement()
    {
        // Validate that we have a rigidbody and the input class
        if (!_Rb || _Input == null)
            return;

        // Get the player input for movement
        Vector2 movementInput = _Input.Player.Move.ReadValue<Vector2>();
        float movementMag = movementInput.magnitude;
        
        // Update the animator if we are moving or not
        if (_Anim)
            _Anim.SetBool("IsMoving", movementMag > 0);
        
        if (movementMag > 0)
        {
            Vector3 forward = transform.forward * movementInput.y;
            Vector3 right = transform.right * movementInput.x;

            Vector3 finalMovement = forward + right;
            _Rb.AddForce(finalMovement * GetMovementSpeed());
        }
    }

    private void DetectOpenDoor()
    {
        RaycastHit hit;
        if (Physics.Raycast(_Cam.transform.position, _Cam.transform.forward, out hit, _RequiredDistanceFromDoor))
        {
            if (hit.collider.CompareTag("IntroDoor"))
            {
                Debug.Log("Hit Intro door");
                IntroDoorController introDoor = hit.collider.GetComponent<IntroDoorController>();
                if(introDoor)
                    introDoor.OpenDoor();
            }
        }
    }

    public void SetWeapon(GameObject weapon, int index)
    {
        if (index == 1)
        {

            if(_SpawnedWeaponOne)
                DestroyImmediate(_SpawnedWeaponOne);

            _SpawnedWeaponOne = Instantiate(weapon, _WeaponAttachPoint);
            if (_SpawnedWeaponOne)
            {
                _WeaponOne = _SpawnedWeaponOne.GetComponent<WeaponController>();
                if(_EquippedWeapon != 1)
                    _SpawnedWeaponOne.SetActive(false);
            }
        }
        else
        {
            if(_SpawnedWeaponTwo)
                DestroyImmediate(_SpawnedWeaponTwo);

            _SpawnedWeaponTwo = Instantiate(weapon, _WeaponAttachPoint);
            if (_SpawnedWeaponTwo)
            {
                _WeaponTwo = _SpawnedWeaponTwo.GetComponent<WeaponController>();
                if(_EquippedWeapon != 2)
                    _SpawnedWeaponTwo.SetActive(false);
            }
        }
    }

    private float GetMovementSpeed()
    {
        if (_IsSprint)
            return _SprintSpeed;

        return _MovementSpeed;
    }

    /// <summary>
    /// Handles the rotation
    /// </summary>
    private void HandleRotation()
    {
        // Validate input and camera
        if (_Input == null || !_Cam)
            return;

        // Get the input value of the mouse
        Vector2 mouseInput = _Input.Player.Look.ReadValue<Vector2>();
        
        // Create the mouse vector indexes multiplying by the sensitivity
        float mouseX = mouseInput.x * (_LookSensitivity * Time.deltaTime);
        float mouseY = mouseInput.y * (_LookSensitivity * Time.deltaTime);

        _XRotation -= mouseY;           // Update the X rotation
        // Clamp the X rotation to prevent circling
        _XRotation = Mathf.Clamp(_XRotation, _ClampXRotationMin, _ClampXRotationMax);
        Quaternion quat = Quaternion.identity;
        quat.eulerAngles = new Vector3(_XRotation, 0f, 0f) + _CurrentRecoil;
        Debug.Log(_CurrentRecoil);
        // Apply rotation to the camera
        _Cam.transform.localRotation = quat;
        // Apply rotation to this object
        this.transform.Rotate(Vector3.up, mouseX);
    }

    private Vector3 CreateRecoil()
    {
        return new Vector3
        {
            x = Random.Range(_MinRecoilY, _MaxRecoilY),
            y = Random.Range(0f, _RecoilX),
            z = 0
        };
        
    }

    private void HandleRecoil()
    {
        if (_ApplyRecoil)
        {
            _CurrentRecoil = Vector3.Slerp(Vector3.zero, _RecoilToApply, _RecoilSpeed * Time.deltaTime);
        }
        else
        {
            _CurrentRecoil = Vector3.Slerp(_CurrentRecoil, Vector3.zero, _ReturnRecoilSpeed * Time.deltaTime);
        }
    }

    public void OnSprint(InputAction.CallbackContext ctx)
    {
        if (_IsReloading)
            return;
        
        _IsSprint = ctx.performed;
        if (_IsAiming)
            _IsAiming = false;
        
        if(_Anim)
            _Anim.SetBool("IsSprinting", _IsSprint);
    }

    public void OnAim(InputAction.CallbackContext ctx)
    {
        _IsAiming = ctx.performed;
        if (_IsSprint)
            _IsSprint = false;
        
        if(_Anim)
            _Anim.SetBool("IsAiming", _IsAiming);
    }

    public void OnFire(InputAction.CallbackContext ctx)
    {
        if (ctx.performed)
        {
            _IsFiring = true;
            _IsSprint = false;
        }
        else
        {
            _IsFiring = false;
        }
    }

    public void OnReload(InputAction.CallbackContext ctx)
    {
        if (_IsSprint)
            _IsSprint = false;
        if (ctx.performed)
        {
            _IsReloading = true;
            if (_EquippedWeapon == 1)
            {
                if (_WeaponOne && _WeaponOne.isActiveAndEnabled)
                {
                    if(_Anim)
                        _Anim.SetTrigger("Reload");
                    
                    _WeaponOne.Reload();
                }
            }
        }
    }

    public void FinishReload()
    {
        _IsReloading = false;
        if (_EquippedWeapon == 1)
        {
            if (_WeaponOne && _WeaponOne.isActiveAndEnabled)
            {
                _WeaponOne.FinishReload();
            }
        }
    }

    private IEnumerator StopRecoil(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        _ApplyRecoil = false;
    }
}
