using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField] private float _DestroyTime;
    [SerializeField] private float _BulletSpeed;

    [SerializeField] private Rigidbody _Rb;

    private float _DamagePoints;
    
    private void Awake()
    {
        if (!_Rb)
            _Rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        StartCoroutine(DestroyTimer());
    }

    private IEnumerator DestroyTimer()
    {
        yield return new WaitForSeconds(_DestroyTime);
        DestroyImmediate(this.gameObject);
    }
    
    public void ShootBullet(Vector3 force, float damagePoints)
    {
        _Rb.AddForce(force * _BulletSpeed, ForceMode.Force);
        _DamagePoints = damagePoints;
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col != null)
        {
            if (col.CompareTag("Enemy"))
            {
                AIController controller = col.GetComponent<AIController>();
                if (controller != null)
                {
                    controller.TakeDamage(_DamagePoints);
                }
            }
            
            Destroy(this.gameObject);
        }
    }
}
