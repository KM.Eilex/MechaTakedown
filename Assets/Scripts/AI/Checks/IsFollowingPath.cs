﻿using UnityEngine;
using System.Collections.Generic;

public class IsFollowingPath : Task
{
    public IsFollowingPath(BehaviorTree tree) : base(tree)
    {
    }

    public IsFollowingPath(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree)
            return ETaskState.FAILURE;

        if (_Tree.GetBlackboard().GetValueAsBool("FollowPath"))
            return ETaskState.SUCCESS;

        return ETaskState.FAILURE;
    }
}