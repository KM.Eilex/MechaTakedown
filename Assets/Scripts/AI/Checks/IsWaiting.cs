﻿using UnityEngine;
using System.Collections.Generic;

public class IsWaiting : Task
{
    public IsWaiting(BehaviorTree tree) : base(tree)
    {
    }

    public IsWaiting(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree || _Tree.GetBlackboard() == null)
            return ETaskState.FAILURE;

        if (_Tree.GetBlackboard().GetValueAsBool("Wait"))
            return ETaskState.SUCCESS;

        return ETaskState.FAILURE;
    }
}