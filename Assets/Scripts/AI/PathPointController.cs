﻿using UnityEngine;
using System.Collections.Generic;
public class PathPointController : MonoBehaviour
{
    private List<Vector3> _PathPoints = new List<Vector3>();                // Reference to all the path points
    [SerializeField] private bool _Circle;                          // Direction of movement for this path point

    public bool Circle => _Circle;
    public int PathPointCount => _PathPoints.Count;

    private void Awake()
    {
        for (int i = 0; i < this.transform.childCount; ++i)
        {
            _PathPoints.Add(this.transform.GetChild(i).position);
        }
    }

    public Vector3 GetPointAtIndex(int index)
    {
        if (index < _PathPoints.Count)
            return _PathPoints[index];

        return Vector3.zero;
    }
}
