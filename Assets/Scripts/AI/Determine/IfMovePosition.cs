﻿using UnityEngine;
using System.Collections.Generic;

public class DetermineMovePosition : Task
{
    private float _MovePositionChance;
    public DetermineMovePosition(BehaviorTree tree, float movePosChance = 0.2f) : base(tree)
    {
        _MovePositionChance = movePosChance;
    }

    public DetermineMovePosition(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree || _Tree.GetBlackboard() == null)
            return ETaskState.FAILURE;

        float randValue = Random.Range(0f, 1f);
        if (randValue <= _MovePositionChance)
        {
            _Tree.GetBlackboard().SetValueAsBool("WantsToMoveLocation", true);
        }

        return ETaskState.SUCCESS;
    }
}