﻿using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class RecoilComponent : MonoBehaviour
{
    private Vector3 _StartRotation;

    [Header("Recoil Settings")] 
    [SerializeField] private float _MinRecoilY;
    [SerializeField] private float _MaxRecoilY;
    [SerializeField] private float _RecoilX;
    [SerializeField] private float _RecoilSpeed;
    [SerializeField] private float _ReturnRecoilSpeed;

    private Vector3 _RecoilToApply;
    [SerializeField] private Transform _PlayerArms;

    private bool _ApplyRecoil;

    private void Start()
    {
        _StartRotation = _PlayerArms.localEulerAngles;
    }

    private void Update()
    {
        if (_ApplyRecoil)
        {
            Vector3 finalRotation = _PlayerArms.localEulerAngles + _RecoilToApply;
            Quaternion quatRotation = Quaternion.identity;
            quatRotation.eulerAngles = finalRotation;
            _PlayerArms.localRotation = Quaternion.Slerp(_PlayerArms.localRotation, quatRotation, _RecoilSpeed * Time.deltaTime);
        }
        else
        {
            Quaternion startRotation = quaternion.identity;
            startRotation.eulerAngles = _StartRotation;
            _PlayerArms.localRotation = Quaternion.Slerp(_PlayerArms.localRotation, startRotation, _ReturnRecoilSpeed * Time.deltaTime);
            if(_PlayerArms.localRotation == startRotation)
                Debug.Log("At Start Rotation");
        }
    }

    public void CreateRecoil()
    {
        _RecoilToApply = new Vector3
        {
            x = Random.Range(0f, _RecoilX),
            y = Random.Range(_MinRecoilY, _MaxRecoilY),
            z = 0f
        };
        _ApplyRecoil = true;
    }

    public void StopRecoil() => _ApplyRecoil = false;
}