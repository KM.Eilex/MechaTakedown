using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroDoorController : MonoBehaviour
{
    [SerializeField] private Animator _Anim;
    private bool _CanOpen = true;

    public void OpenDoor()
    {
        if(_Anim)
            _Anim.SetBool("DoorUp", true);

        
    }

    public void TriggerCloseDoor()
    {
        StartCoroutine(CloseDoor());
        _CanOpen = false;
    }

    private IEnumerator CloseDoor()
    {
        yield return new WaitForSeconds(2);
        if(_Anim)
            _Anim.SetBool("DoorUp", false);
    }
}
