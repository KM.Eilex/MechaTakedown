﻿using UnityEngine;
using System.Collections.Generic;

public class GetPathPointTask : Task
{
    private const float REACHED_DISTANCE = 1.0f;
    
    public GetPathPointTask(BehaviorTree tree) : base(tree)
    {
    }

    public GetPathPointTask(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree)
            return ETaskState.FAILURE;

        Blackboard board = _Tree.GetBlackboard();
        if (board != null)
        {
            int index = board.GetValueAsInt("CurrentPathIndex");
            AIController controller = _Tree.Owner;
            if (controller != null)
            {
                Vector3 moveToPoint = controller.Path.GetPointAtIndex(index);
                board.SetValueAsVector("MoveToLocation", moveToPoint);
                return ETaskState.SUCCESS;
            }
        }

        return ETaskState.FAILURE;
    }
}