﻿using UnityEngine;
using System.Collections.Generic;

public class WaitTask : Task
{
    private float _WaitTime;
    private float _CurrentTime;
    
    public WaitTask(BehaviorTree tree, float waitTime = 5.0f) : base(tree)
    {
        _WaitTime = waitTime;
    }

    public WaitTask(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        _CurrentTime += 1 * Time.deltaTime;

        if (_CurrentTime > _WaitTime)
        {
            _WaitTime = 0f;
            _Tree.GetBlackboard().SetValueAsBool("Wait", false);
            return ETaskState.SUCCESS;
        }

        return ETaskState.RUNNING;
    }
}