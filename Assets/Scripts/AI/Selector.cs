﻿using System.Collections.Generic;
using UnityEngine;

public class Selector : Task
{
    public Selector(BehaviorTree tree) : base(tree)
    {
    }

    public Selector(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        foreach (var task in _ChildrenTask)
        {
            if (task != null)
            {
                switch (task.RunTask())
                {
                    case ETaskState.FAILURE:
                        continue;
                    case ETaskState.RUNNING:
                        return ETaskState.RUNNING;
                    case ETaskState.SUCCESS:
                        return ETaskState.SUCCESS;
                }
            }
        }

        return ETaskState.FAILURE;
    }
}