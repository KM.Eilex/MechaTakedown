﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum ETaskState
{
    SUCCESS = 0,
    FAILURE = 1,
    RUNNING = 2
}

public abstract class Task
{
    protected BehaviorTree _Tree;
    protected List<Task> _ChildrenTask = new List<Task>();
    public Task Parent;

    public Task(BehaviorTree tree)
    {
        _Tree = tree;
    }

    public Task(BehaviorTree tree, List<Task> children)
    {
        _Tree = tree;
        AddChildren(children);
    }

    public void Attach(Task child)
    {
        if (child != null)
        {
            _ChildrenTask.Add(child);
            child.Parent = this;
        }
    }

    public void AddChildren(List<Task> children)
    {
        foreach(var child in children)
            Attach(child);
    }

    public abstract ETaskState RunTask();
}