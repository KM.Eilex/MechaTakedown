﻿using UnityEngine;

public abstract class BehaviorTree : MonoBehaviour
{
    protected Task _RootTask;

    public bool _IsActive = true;
    protected bool _HasInitialized = false;

    protected Blackboard _BlackboardRef;

    protected AIController _Owner;
    public AIController Owner => _Owner;

    protected virtual void Awake()
    {
        _RootTask = CreateTree();
    }

    public void Initialize(Blackboard board, AIController owner)
    {
        _BlackboardRef = board;
        _HasInitialized = true;
        _Owner = owner;
    }

    protected virtual void Update()
    {
        if (!_HasInitialized)
        {
            Debug.LogWarning("Behavior tree not initialized");
            return;
        }
        if (_RootTask != null && _IsActive)
        {
            _RootTask.RunTask();
        }
    }

    public Blackboard GetBlackboard() => _BlackboardRef;
    
    protected abstract Task CreateTree();
}