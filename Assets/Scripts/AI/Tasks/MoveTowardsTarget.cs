﻿using UnityEngine;
using System.Collections.Generic;

public class MoveTowardsTarget : Task
{
    public MoveTowardsTarget(BehaviorTree tree) : base(tree)
    {
    }

    public MoveTowardsTarget(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree)
            return ETaskState.FAILURE;

        Blackboard board = _Tree.GetBlackboard();
        if (board != null)
        {
            GameObject target = board.GetValueAsGameObject("Target");
            if (target)
            {
                Vector3 targetPosition = target.transform.position;
                board.SetValueAsVector("MoveToLocation", targetPosition);
                return ETaskState.SUCCESS;
            }
        }

        return ETaskState.FAILURE;
    }
}