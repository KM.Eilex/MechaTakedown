﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

public class SpawnDoorController : MonoBehaviour
{
    private Animator _Anim;
    public bool DoorIsActive = false;

    public Transform SpawnPoint;

    private void Awake()
    {
        _Anim = GetComponent<Animator>();
    }

    public void OpenDoor()
    {
        if (!DoorIsActive)
            return;
        
        _Anim.SetBool("DoorUp", true);
        StartCoroutine(CloseDoor());

    }

    private IEnumerator CloseDoor()
    {
        yield return new WaitForSeconds(10f);
        _Anim.SetBool("DoorUp", false);
    }
}