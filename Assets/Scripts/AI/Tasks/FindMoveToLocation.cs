﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem.LowLevel;

public class FindMoveToLocation : Task
{
    private const float _SearchRange = 50f;
    public FindMoveToLocation(BehaviorTree tree) : base(tree)
    {
    }

    public FindMoveToLocation(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree)
            return ETaskState.FAILURE;

        if (!_Tree.GetBlackboard().GetValueAsBool("HasMoveToLocation"))
        {
            Vector3 currentPos = _Tree.Owner.transform.position;
            Vector3 randomPoint = currentPos + Random.insideUnitSphere * _SearchRange;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                _Tree.GetBlackboard().SetValueAsVector("MoveToLocation", hit.position);
                return ETaskState.SUCCESS;
            }
        }
        else
        {
            return ETaskState.SUCCESS;
        }
        return ETaskState.FAILURE;
    }
}