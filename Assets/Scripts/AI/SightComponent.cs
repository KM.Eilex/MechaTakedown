﻿using UnityEngine;
using System.Collections.Generic;

public class SightComponent : MonoBehaviour
{
    private GameObject _Player;
    [SerializeField] private float _SightDistance = 30;

    private AIController _Owner;

    private void Awake()
    {
        if (!_Player)
            _Player = GameObject.FindGameObjectWithTag("Player");

        _Owner = GetComponent<AIController>();
    }

    private void Update()
    {
        if (CheckPlayerInFront())
        {
            if (!_Owner.BlackboardRef.GetValueAsGameObject("Target"))
            {
                if (Vector3.Distance(this.transform.position, _Player.transform.position) < _SightDistance)
                {
                    _Owner.BlackboardRef.SetValueAsGameObject("Target", _Player);
                }
            }
        }
    }

    private bool CheckPlayerInFront()
    {
        if (!_Player)
            return false;

        Vector3 forward = this.transform.TransformDirection(Vector3.forward);
        Vector3 toPlayer = _Player.transform.position - this.transform.position;

        if (Vector3.Dot(forward, toPlayer) > 0)
            return true;

        return false;
    }
}