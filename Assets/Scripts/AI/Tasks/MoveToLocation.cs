﻿using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MoveToLocation : Task
{
    private const float STOPPING_DISTANCE = 1.0f;
    public MoveToLocation(BehaviorTree tree) : base(tree)
    {
    }

    public MoveToLocation(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        if (!_Tree)
            return ETaskState.FAILURE;

        Blackboard board = _Tree.GetBlackboard();
        if (board != null)
        {
            Vector3 moveToLocation = board.GetValueAsVector("MoveToLocation");
            Vector3 aiPosition = _Tree.Owner.transform.position;

            if (Vector3.Distance(moveToLocation, aiPosition) > STOPPING_DISTANCE)
            {
                _Tree.Owner.MoveTo(moveToLocation);
                return ETaskState.RUNNING;
            }
            else
            {
                _Tree.Owner.StopMoving();
                board.SetValueAsBool("HasMoveToLocation", false);
                board.SetValueAsVector("MoveToLocation", Vector3.zero);
                return ETaskState.SUCCESS;
            }
        }

        return ETaskState.FAILURE;
    }
}