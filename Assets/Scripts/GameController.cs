using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class GameController : MonoBehaviour
{
    public static GameController Instance;              // Singleton

    private int _CurrentRound = 1;
    
    [Header("Round Loading")]
    private bool _RoundLoading = true;
    private float _RoundWaitTime = 40;
    private float _CurrentTime;

    [SerializeField] private UnityEvent StartRound;
    [SerializeField] private UnityEvent FinishRound;

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Update()
    {
        if (_RoundLoading)
        {
            _CurrentTime += 1 * Time.deltaTime;
            if (_CurrentTime > _RoundWaitTime)
            {
                _RoundLoading = false;
                _CurrentTime = 0f;
                
                StartRound?.Invoke();
            }
        }
    }

    public void OnFinishRound()
    {
        FinishRound?.Invoke();
    }
}
