﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnController : MonoBehaviour
{
    public static SpawnController Instance;
    [Header("Round Settings")] 
    [SerializeField] private float _RoundEnemiesModifier = 1.15f;
    [SerializeField] private int _CurrentSpawningRound;             // How many enemies will spawn this round
    
    private List<GameObject> _SpawnedEnemies = new List<GameObject>();
    
    [Header("Spawn Settings")]
    [SerializeField] private List<SpawnDoorController> _SpawningDoors = new List<SpawnDoorController>();            // Reference to all the doors that can spawn
    [SerializeField] private float _MinNextSpawn;                       // Min amount of time before the next enemy spawns
    [SerializeField] private float _MaxNextSpawn;                       // Max amount of time before the next enemy spawns
    [SerializeField] private float _CurrentSpawnTime;                   // When the next enemy will spawn
    private float _CurrentTime;                 // where the spawn timer is currently at
    private bool _IsSpawningEnemies = false;                // If we are spawning enemeies

    [Header("Enemies")] 
    [SerializeField] private GameObject _Cobra;

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else 
            Destroy(this.gameObject);
    }

    public void AddDoor(SpawnDoorController door)
    {
        _SpawningDoors.Add(door);
        door.DoorIsActive = true;
    }
    
    public void OnStartRound()
    {
        float enemiesToSpawn = _CurrentSpawningRound * _RoundEnemiesModifier;           // Determine how many enemies to spawn
        _CurrentSpawningRound = Mathf.FloorToInt(enemiesToSpawn);                   // Make the amount int
        _IsSpawningEnemies = true;                  // Enable spawning
    }

    private void Update()
    {
        // Check that we are spawning enemies
        if (_IsSpawningEnemies)
        {
            _CurrentTime += 1 * Time.deltaTime;                 // Increment the timer
            // Check if the timer has complete
            if (_CurrentTime > _CurrentSpawnTime)
            {
                SpawnEnemy();                   // Spawn the enemy
                _CurrentTime = 0f;              // Reset the timer
                _CurrentSpawnTime = Random.Range(_MinNextSpawn, _MaxNextSpawn);             // Get the next spawn time
                
                // Check if we are finished spawning enemies
                if (_SpawnedEnemies.Count == _CurrentSpawningRound)
                    _IsSpawningEnemies = false;
            }
        }
    }

    private void SpawnEnemy()
    {
        GameObject go = Instantiate(_Cobra);                    // Spawn the object
        SpawnDoorController spawningDoor = _SpawningDoors[Random.Range(0, _SpawningDoors.Count)];
        if (spawningDoor != null)
        {
            spawningDoor.OpenDoor();
            go.transform.position = spawningDoor.SpawnPoint.position;
            _SpawnedEnemies.Add(go);
        }
    }

    public void CheckEnemyCount()
    {
        foreach (var enemy in _SpawnedEnemies)
        {
            AIController ai = enemy.GetComponent<AIController>();
            if (ai.IsAlive)
            {
                return;
            }
        }
        
        GameController.Instance.OnFinishRound();
    }
}