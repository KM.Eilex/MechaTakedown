﻿using UnityEngine;

public enum EWeaponType
{
    PISTOL,
    RIFLE,
    SHOTGUN,
    SNIPPER
}

public enum EFireType
{
    SINGLE,
    BURST,
    AUTO
}

[CreateAssetMenu]
public class WeaponData : ScriptableObject
{
    
}