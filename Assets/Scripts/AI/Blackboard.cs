﻿using System.Collections.Generic;
using UnityEngine;

public class Blackboard
{
    private List<BlackboardProperty> _Properties = new List<BlackboardProperty>();

    public void SetValueAsInt(string key, int value)
    {
        foreach (var prop in _Properties)
        {
            if (prop == key)
            {
                if (prop is IntProperty intProp)
                {
                    intProp.Value = value;
                    return;
                }
            }
        }
        
        _Properties.Add(new IntProperty(key, value));
    }

    public int GetValueAsInt(string key)
    {
        foreach(var prop in _Properties)
            if(prop == key)
                if (prop is IntProperty intProp)
                    return intProp.Value;

        return 0;
    }

    public void SetValueAsFloat(string key, float value)
    {
        foreach (var prop in _Properties)
        {
            if (prop == key)
            {
                if (prop is FloatProperty floatProp)
                {
                    floatProp.Value = value;
                    return;
                }
            }
        }

        _Properties.Add(new FloatProperty(key, value));
    }

    public float GetValueAsFloat(string key)
    {
        foreach(var prop in _Properties)
            if(prop == key)
                if (prop is FloatProperty floatProp)
                    return floatProp.Value;

        return 0f;
    }

    public void SetValueAsBool(string key, bool value)
    {
        foreach (var prop in _Properties)
        {
            if (prop == key)
            {
                if (prop is BoolProperty bProp)
                {
                    bProp.Value = value;
                    return;
                }
            }
        }
        
        _Properties.Add(new BoolProperty(key, value));
    }

    public bool GetValueAsBool(string key)
    {
        foreach(var prop in _Properties)
            if(prop == key)
                if (prop is BoolProperty bProp)
                    return bProp.Value;


        return false;
    }

    public void SetValueAsGameObject(string key, GameObject value)
    {
        foreach (var prop in _Properties)
        {
            if (prop == key)
            {
                if (prop is GameObjProperty goProp)
                {
                    goProp.Value = value;
                    return;
                }
            }
        }
        
        _Properties.Add(new GameObjProperty(key, value));
    }

    public GameObject GetValueAsGameObject(string key)
    {
        foreach (var prop in _Properties)
        {
            if(prop == key)
                if (prop is GameObjProperty goProp)
                    return goProp.Value;
        }

        return null;
    }

    public void SetValueAsVector(string key, Vector3 value)
    {
        foreach (var prop in _Properties)
        {
            if (prop == key)
            {
                if (prop is VectorProperty vecProp)
                {
                    vecProp.Value = value;
                    return;
                }
            }
        }
        
        
        _Properties.Add(new VectorProperty(key, value));
    }

    public Vector3 GetValueAsVector(string key)
    {
        foreach(var prop in _Properties)
            if(prop == key)
                if (prop is VectorProperty vecProp)
                    return vecProp.Value;

        return Vector3.zero;
    }
}

public class BlackboardProperty
{
    public string Key;

    public BlackboardProperty(string key)
    {
        Key = key;
    }

    public static bool operator ==(BlackboardProperty a, string b)
    {
        if (a.Key == b)
            return true;

        return false;
    }

    public static bool operator !=(BlackboardProperty a, string b)
    {
        return a != b;
    }
}

public class IntProperty : BlackboardProperty
{
    public int Value;

    public IntProperty(string key, int value) : base(key)
    {
        Value = value;
    }
}

public class FloatProperty : BlackboardProperty
{
    public float Value;

    public FloatProperty(string key, float value) : base(key)
    {
        Value = value;
    }
}

public class BoolProperty : BlackboardProperty
{
    public bool Value;

    public BoolProperty(string key, bool value) : base(key)
    {
        Value = value;
    }
}

public class GameObjProperty : BlackboardProperty
{
    public GameObject Value;

    public GameObjProperty(string key, GameObject value) : base(key)
    {
        Value = value;
    }
}

public class VectorProperty : BlackboardProperty
{
    public Vector3 Value;

    public VectorProperty(string key, Vector3 value) : base(key)
    {
        Value = value;
    }
}