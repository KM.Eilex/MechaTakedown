﻿using System.Collections.Generic;
using UnityEngine;

public class IsInAttackRange : Task
{
    public IsInAttackRange(BehaviorTree tree) : base(tree)
    {
    }

    public IsInAttackRange(BehaviorTree tree, List<Task> children) : base(tree, children)
    {
    }

    public override ETaskState RunTask()
    {
        // Validate the tree
        if (!_Tree)
            return ETaskState.FAILURE;

        Blackboard board = _Tree.GetBlackboard();               // Get reference to the blackboard
        if (board != null)
        {
            // Get reference to the required distances
            float minDistance = board.GetValueAsFloat("MinAttackDistance");
            float maxDistance = board.GetValueAsFloat("MaxAttackDistance");
            // Get position of owner and the target=
            Vector3 aiPosition = _Tree.Owner.transform.position;
            Vector3 targetPosition = board.GetValueAsGameObject("Target").transform.position;
            // Get distance to target and check if in range
            float distanceToTarget = Vector3.Distance(aiPosition, targetPosition);
            if (distanceToTarget > minDistance && distanceToTarget < maxDistance)
                return ETaskState.SUCCESS;

            return ETaskState.FAILURE;

        }

        return ETaskState.FAILURE;
    }
}